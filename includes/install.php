<?php
/**
 * Install class.
 * 
 * * @package Woo_External_Cart
 */

namespace WCXCart;

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) )
{
    exit;
}

/**
 * Install class.
 *
 * @since 1.0.0
 */
class Install
{
    /**
     * Class instance.
     *
     * @since 1.0.0
     * @access private
     * @static
     *
     * @var self
     */
    private static $_instance = null;

    /**
     * Ensures only one instance of the class is loaded or can be loaded.
     *
     * @since 1.0.0
     * @access public
     * @static
     *
     * @return self An instance of the class.
     */
    public static function instance()
    {
        if ( is_null( self::$_instance ) )
        {
            self::$_instance = new self();
        }
        return self::$_instance;
    }
    
    /**
     * Constructor.
     *
     * @since 1.0.0
     * @access private
     */
    private function __construct()
    {
        add_action( 'init', [ $this, 'init' ], 5 );
    }

    function init()
    {
        self::install();
    }

    private static function install()
    {
        global $wpdb;

        if ( ! function_exists( 'dbDelta' ) )
        {
            require_once ABSPATH . 'wp-admin/includes/upgrade.php';
        }

        $collate = '';

        if ( $wpdb->has_cap( 'collation' ) )
        {
            $collate = $wpdb->get_charset_collate();
        }

        $max_index_length = 191;

    $tables = "
CREATE TABLE {$wpdb->prefix}wooxcart_items (
    item_id BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
    item_sku varchar(255) NOT NULL,
    item_data longtext,
    item_options longtext,
    PRIMARY KEY (item_id)
) $collate;";
        \dbDelta( $tables );
    }
}

Install::instance();