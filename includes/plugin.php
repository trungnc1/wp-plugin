<?php
/**
 * Main plugin class.
 * 
 * * @package Woo_External_Cart
 */

namespace WCXCart;

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) )
{
    exit;
}

/**
 * System plugin.
 *
 * @since 1.0.0
 */
class Plugin
{
    /**
     * Class instance.
     *
     * @since 1.0.0
     * @access private
     * @static
     *
     * @var self
     */
    private static $_instance = null;

    /**
     * Store all requested cart items
     *
     * @var array
     * @access private
     */
    private $cart_calculated_cart_items = [];

    /**
     * Ensures only one instance of the class is loaded or can be loaded.
     *
     * @since 1.0.0
     * @access public
     * @static
     *
     * @return self An instance of the class.
     */
    public static function instance()
    {
        if ( is_null( self::$_instance ) )
        {
            self::$_instance = new self();
        }
        return self::$_instance;
    }
    
    /**
     * Constructor.
     *
     * @since 1.0.0
     * @access private
     */
    private function __construct()
    {
        require WCXCART_PATH . 'includes/helpers.php';

        add_action( 'rest_api_init', [ $this, 'rest_api_init' ] );
        // add_action( 'init', [ $this, 'process_request' ] );

        add_action( 'woocommerce_before_calculate_totals', [ $this, 'woocommerce_before_calculate_totals' ], 50 );
        add_filter( 'woocommerce_cart_item_price', [ $this, 'woocommerce_cart_item_price' ], 50, 3 );

        add_action( 'woocommerce_after_cart_item_name', [ $this, 'show_additional_options' ], 10, 2 );
    }

    function process_request()
    {
        if ( is_admin() )
        {
            return;
        }

        /* echo json_encode( [
            'product' => 'woo-album',
            'price'   => '26.37',
            'options' => [
                'Frame Type'   => 'Oak',
                'Colour Space' => 'adobeRBG',
                'Comment'      => 'Test',
            ],
            'photos'   => '{random_hash}',
            'quantity' => 2,
        ] ); */

        if ( ! isset( $_GET['wooxcart'] ) || $_GET['wooxcart'] !== 'create' )
        {
            return;
        }

        if ( empty( $_POST['data'] ) )
        {
            return;
        }

        $data = json_decode( wp_unslash( $_POST['data'] ), true );
        if ( json_last_error() !== JSON_ERROR_NONE )
        {
            return;
        }

        var_dump( $data );
        // var_dump( $_GET );
        // var_dump( $_POST );
    }

    function rest_api_init()
    {
        register_rest_route( 'woo-external-cart/v1', '/create-cart', array(
            'methods'  => 'POST',
            'callback' => [ $this, 'create_cart_from_request' ],
            // 'permission_callback' => [ $this, 'permission_callback' ]
        ) );
    }

    /**
     * Undocumented function
     *
     * @param \WP_Rest_Request $request
     * @return void
     */
    function permission_callback( $request )
    {
        $params  = $request->get_params();
        $headers = $request->get_headers();

        if ( ! is_array( $params ) || empty( $headers['api_key'] ) )
        {
            return false;
        }

        /**
         * This will go to setting page
         */
        $secret = 'abcxyz';

        $request_api_key = base64_decode( $headers['api_key'][0] );
        $hashed_api_key  = hash_hmac( 'sha256', json_encode( $params ), $secret, true );

        if ( ! hash_equals( $hashed_api_key, $request_api_key ) )
        {
            return false;
        }

        return true;
    }

    /**
     * Undocumented function
     *
     * @param \WP_Rest_Request $request
     * @return void
     */
    function create_cart_from_request( $request )
    {
        $results = [
            'status' => 'failed',
            'error_code' => '',
            'error_msg' => ''
        ];

        $params = $request->get_params();

        $data = wp_parse_args( $params, [
            'product'  => '', // SKU
            'price'    => '',
            'options'  => [],
            'photos'   => '',
            'quantity' => 0
        ] );

        if ( empty( $data['product'] ) || $data['quantity'] <= 0 )
        {
            $results['error_code'] = 'invalid_qty';
            $results['error_msg'] = 'Invalid product quantity.';
            echo json_encode( $results );
            exit;
        }

        $product_id = wc_get_product_id_by_sku( $data['product'] );
        if ( ! $product_id )
        {
            $results['error_code'] = 'not_exist';
            $results['message'] = 'Product ' . $data['product'] . ' does not exist.';
            echo json_encode( $results );
            return;
        }

        $cart = WC()->cart;
        $xcart_item_id = $this->set_xcart_item_data( $data );

        if ( !! $cart->add_to_cart( $product_id, $data['quantity'], 0, [], [ 'wooxcart_item_id' => $xcart_item_id ] ) )
        {
            $results['status'] = 'success';
            $results['message'] = 'Successfully added ' . $data['product'] . ' x ' . $data['quantity'] . ' to cart.';
        }
        else
        {
            $results['error_code'] = 'add_to_cart_failed';
            $results['message'] = 'Could not add product ' . $data['product'] . ' to cart.';
        }
        
        echo json_encode( $results );
        exit;
    }

    function set_xcart_item_data( $data )
    {
        $options = $data['options'];
        unset( $data['options'] );

        /**
         * @var \wpdb $wpdb
         */
        global $wpdb;

        $wxc_products = $wpdb->get_results(
            $wpdb->prepare(
                "SELECT * FROM {$wpdb->prefix}wooxcart_items WHERE `item_sku`=%s AND `item_options`=%s LIMIT 1",
                $data['product'],
                maybe_serialize( $options )
            ),
            ARRAY_A
        );

        if ( $wxc_products )
        {
            return $wxc_products[0]['item_id'];
        }

        $wpdb->insert(
            "{$wpdb->prefix}wooxcart_items",
            [
                'item_sku' => $data['product'],
                'item_data' => maybe_serialize( $data ),
                'item_options' => maybe_serialize( $options )
            ]
        );

        return $wpdb->insert_id;
    }

    function get_product_data( $cart_item )
    {
        if ( ! isset( $cart_item['wooxcart_item_id'] ) )
        {
            return false;
        }

        $data = [
            'product'  => '', // SKU
            'price'    => '',
            'options'  => [],
            'photos'   => '',
            'quantity' => 0
        ];

        /**
         * @var \wpdb $wpdb
         */
        global $wpdb;

        $wxc_products = $wpdb->get_results(
            $wpdb->prepare( "SELECT * FROM {$wpdb->prefix}wooxcart_items WHERE `item_id`=%d LIMIT 1", $cart_item['wooxcart_item_id'] ),
            ARRAY_A
        );

        if ( ! $wxc_products )
        {
            return false;
        }

        $item_data = maybe_unserialize( $wxc_products[0]['item_data'] );
        $item_data['options'] = maybe_unserialize( $wxc_products[0]['item_options'] );

        return wp_parse_args( $item_data, $data );
    }

    protected function fake_get_variable_product_data()
    {
        $data = [
            'product' => 'insta_frames',
            'price'   => '26.37',
            'options' => [
                'Frame Type'   => 'Oak',
                'Colour Space' => 'adobeRBG',
                'Comment'      => 'Test',
            ],
            'photos'   => '{random_hash}',
            'quantity' => 2,
        ];
        return $data;
    }

    /**
     * Undocumented function
     *
     * @param \WC_Cart $wc_cart
     * @return void
     */
    function woocommerce_before_calculate_totals( $wc_cart )
    {
        if ( is_admin() && ! defined( 'DOING_AJAX' ) )
        {
            return;
        }

        /* var_dump( $this->cart_calculated_products );
        var_dump( did_action( 'woocommerce_before_calculate_totals' ) );

        if ( did_action( 'woocommerce_before_calculate_totals' ) > 2 )
        {
            return;
        } */

        $cart = $wc_cart->get_cart();

        foreach ( $cart as $hash => $cart_item )
        {
            if ( in_array( $cart_item['key'], $this->cart_calculated_cart_items ) )
            {
                continue;
            }

            $this->cart_calculated_cart_items[] = $cart_item['key'];

            /**
             * @var \WC_Product
             */
            $_product = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $hash );

            $product_data = $this->get_product_data( $cart_item );

            if ( ! $product_data )
            {
                continue;
            }

            if ( 'instock' !== $_product->get_stock_status() )
            {
                continue;
            }

            if ( $_product->get_manage_stock() && $_product->get_stock_quantity() < $product_data['quantity'] )
            {
                continue;
            }

            $_product->set_price( $product_data['price'] );
        }
    }

    /**
     * Change item price in cart.
     *
     * @param string $price
     * @param array  $cart_item
     * @param string $cart_item_key
     * @return string
     */
    function woocommerce_cart_item_price( $price, $cart_item, $cart_item_key )
    {
        /**
         * @var \WC_Product
         */
        $_product     = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );
        $product_data = $this->get_product_data( $cart_item );

        if ( ! $product_data )
        {
            return $price;
        }

        $_product->set_price( $product_data['price'] );
        return WC()->cart->get_product_price( $_product );
    }

    function show_additional_options( $cart_item, $cart_item_key )
    {
        $_product = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );
        if ( $_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters( 'woocommerce_cart_item_visible', true, $cart_item, $cart_item_key ) )
        {
            $product_data = $this->get_product_data( $cart_item );
            if ( ! $product_data )
            {
                return;
            }
            if ( $product_data['options'] )
            {
                $opt_html = [];
                foreach( $product_data['options'] as $key => $opt )
                {
                    $opt_html[] = sprintf(
                        '<span class="opt-name">%1$s</span>: <span class="opt-val">%2$s</span>',
                        esc_html( $key ),
                        esc_html( $opt )
                    );
                }
                echo '<br>' . implode( '<br>', $opt_html );
            }
        }
    }
}

Plugin::instance();