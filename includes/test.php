<?php
/**
 * Test class.
 * 
 * * @package Woo_External_Cart
 */

namespace WCXCart;

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) )
{
    exit;
}

/**
 * Test class.
 *
 * @since 1.0.0
 */
class Test
{
    /**
     * Class instance.
     *
     * @since 1.0.0
     * @access private
     * @static
     *
     * @var self
     */
    private static $_instance = null;

    /**
     * Ensures only one instance of the class is loaded or can be loaded.
     *
     * @since 1.0.0
     * @access public
     * @static
     *
     * @return self An instance of the class.
     */
    public static function instance()
    {
        if ( is_null( self::$_instance ) )
        {
            self::$_instance = new self();
        }
        return self::$_instance;
    }
    
    /**
     * Constructor.
     *
     * @since 1.0.0
     * @access private
     */
    private function __construct()
    {
        add_action( 'woocommerce_before_add_to_cart_button', [ $this, 'before_add_to_cart_button' ] );
        add_filter( 'woocommerce_add_cart_item_data', [ $this, 'add_cart_item_data' ], 10, 3 );
        add_filter( 'woocommerce_get_item_data', [ $this, 'get_item_data' ], 10, 2 );
    }

    /**
     * Output engraving field.
     */
    function before_add_to_cart_button()
    {
        global $product;

        if ( $product->get_id() !== 27 )
        {
            return;
        }

        ?>
        <div class="iconic-engraving-field">
            <label for="iconic-engraving"><?php _e( 'Engraving (10 characters)', 'iconic' ); ?></label>
            <input type="text" id="iconic-engraving" name="iconic-engraving" placeholder="<?php _e( 'Enter engraving text', 'iconic' ); ?>" maxlength="10">
        </div>
        <?php
    }

    /**
     * Add engraving text to cart item.
     *
     * @param array $cart_item_data
     * @param int   $product_id
     * @param int   $variation_id
     *
     * @return array
     */
    function add_cart_item_data( $cart_item_data, $product_id, $variation_id )
    {
        $engraving_text = filter_input( INPUT_POST, 'iconic-engraving' );
        if ( empty( $engraving_text ) )
        {
            return $cart_item_data;
        }
        $cart_item_data['iconic-engraving'] = $engraving_text;
        return $cart_item_data;
    }

    /**
     * Display engraving text in the cart.
     *
     * @param array $item_data
     * @param array $cart_item
     *
     * @return array
     */
    function get_item_data( $item_data, $cart_item )
    {
        if ( empty( $cart_item['iconic-engraving'] ) )
        {
            return $item_data;
        }

        $item_data[] = array(
            'key'     => __( 'Engraving', 'iconic' ),
            'value'   => wc_clean( $cart_item['iconic-engraving'] ),
            'display' => '',
        );

        return $item_data;
    }
}

Test::instance();