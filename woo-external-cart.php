<?php
/**
 * Plugin Name: WooCommerce External Cart
 * Description: Generate cart item data from API.
 * Plugin URI:  https://example.com/
 * Version:     1.0.0
 * Author:      John Doe
 * Author URI:  https://example.com/
 * Text Domain: woo-external-cart
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) )
{
    exit;
}

define( 'WCXCART_BASE', plugin_basename( __FILE__ ) );
define( 'WCXCART_PATH', plugin_dir_path( __FILE__ ) );
define( 'WCXCART_URL', plugins_url( '/', __FILE__ ) );

/**
 * Load textdomain.
 *
 * @since 1.0.0
 *
 * @return void
 */
function wcxcart_load_plugin_textdomain()
{
    load_plugin_textdomain( 'woo-external-cart' );
}
add_action( 'plugins_loaded', 'wcxcart_load_plugin_textdomain' );

/**
 * Hook: plugins_loaded
 *
 * @return void
 */
function wcxcart_plugin_loaded()
{
    if ( ! did_action( 'woocommerce_loaded' ) )
    {
        add_action( 'admin_notices', 'wcxcart_admin_notice_woocommerce_missing' );
        if ( ! function_exists( 'deactivate_plugins' ) )
        {
            include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
            deactivate_plugins( 'woo-external-cart/woo-external-cart.php' );
        }
        return;
    }

    // require WCXCART_PATH . 'includes/install.php';
    require WCXCART_PATH . 'includes/test.php';
    require WCXCART_PATH . 'includes/plugin.php';
}
add_action( 'plugins_loaded', 'wcxcart_plugin_loaded' );

/**
 * Admin notice
 *
 * Warning when the site doesn't have WooCommerce installed or activated.
 *
 * @since 1.0.0
 * @access public
 */
function wcxcart_admin_notice_woocommerce_missing()
{
    if ( isset( $_GET['activate'] ) )
    {
        unset( $_GET['activate'] );
    }
    $message = sprintf(
        /* translators: 1: Plugin name 2: WooCommerce */
        esc_html__( '"%1$s" requires "%2$s" to be installed and activated.', 'woo-external-cart' ),
        '<strong>' . esc_html__( 'Woo External Cart', 'woo-external-cart' ) . '</strong>',
        '<strong>' . esc_html__( 'WooCommerce', 'woo-external-cart' ) . '</strong>'
    );
    printf( '<div class="notice notice-error is-dismissible"><p>%1$s</p></div>', $message );
}